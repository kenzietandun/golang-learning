package main

import "golang.org/x/tour/pic"

func Pic(dx, dy int) [][]uint8 {
  f := make([][]uint8, dy)
  for i := range f {
    f[i] = make([]uint8, dx)
    for j := range f[i] {
      f[i][j] = uint8(i * j)
    }
  }

  return f
}

func main() {
  pic.Show(Pic)
}
