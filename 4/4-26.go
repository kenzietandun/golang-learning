package main

import "fmt"

func fibonacci() func() int {
  curr := 1
  prev := 0
  return func() int {
    curr, prev = curr + prev, curr
    return curr
  }
}

func main() {
  f := fibonacci()
  for i := 0; i < 50; i++ {
    fmt.Println(f())
  }
}
